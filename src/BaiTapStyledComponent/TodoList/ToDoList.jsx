import React, { Component } from "react";
import { ThemeProvider } from "styled-components";

import { ToDoListDarkTheme } from "./../Themes/ToDoListDarkTheme";
import { ToDoListLightTheme } from "./../Themes/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "./../Themes/ToDoListPrimaryTheme";

import { Dropdown } from "./../components/Dropdown";
import {
  Heading1,
  Heading2,
  Heading3,
  Heading4,
  Heading5,
} from "./../components/Heading";
import { TextField, Label, Input } from "./../components/TextField";
import { Button } from "./../components/Button";
import { Container } from "./../components/Container";
import { Table, Thead, Tr, Th } from "./../components/Table";
import { connect } from "react-redux";
import {
  doiTheme,
  themTask,
  xoaTask,
  xongTask,
  hoanTask,
  suaTask,
  capNhatTask,
} from "./../redux/action/toDoListAction";

import { arrTheme } from "../Themes/ThemeManager";
class ToDoList extends Component {
  state = {
    taskName: "",
    disabled: true,
  };
  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-end">
              <Button
                onClick={() => {
                  this.props.handleEditTask(task);
                }}
              >
                <i className="fa fa-edit"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.handleDoneTask(task.id);
                }}
              >
                <i className="fa fa-check"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.handleDeleteTask(task.id);
                }}
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };
  renderTaskComplete = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-end">
              <Button
                onClick={() => {
                  this.props.handleRedoTask(task.id);
                }}
              >
                <i className="fa fa-redo-alt"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.handleDeleteTask(task.id);
                }}
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };
  handleChange = (e) => {
    let { name, value } = e.target;
    this.setState(
      {
        [name]: value,
      },
      () => {
        console.log(this.state.taskName);
      }
    );
  };
  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return (
        <option key={index} value={theme.id}>
          {theme.name}
        </option>
      );
    });
  };
  // componentWillReceiveProps(newProps) {
  //   this.setState({
  //     taskName: newProps.taskEdit.taskName,
  //   });
  // }
  componentDidUpdate(prevProps, prevState) {
    // works

    if (prevProps.taskEdit.id !== this.props.taskEdit.id) {
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
    }
  }

  render() {
    return (
      <ThemeProvider theme={this.props.themeColor}>
        <Container className="w-50">
          <Dropdown
            onChange={(e) => {
              let { value } = e.target;

              this.props.handleChangeTheme(value);
            }}
          >
            {this.renderTheme()}
          </Dropdown>
          <Heading3>To do list</Heading3>
          <TextField
            value={this.state.taskName}
            label="Task name"
            name="taskName"
            className="w-50"
            onChange={this.handleChange}
          ></TextField>
          <Button
            className="ms-2"
            onClick={() => {
              let { taskName } = this.state;
              let newTask = {
                id: Date.now(),
                taskName: taskName,
                done: false,
              };
              this.props.handleAddTask(newTask);
            }}
          >
            <i className="fa fa-plus"></i>Add task
          </Button>
          {/* nút cập nhật task  */}
          {this.state.disabled ? (
            <Button
              disabled
              onClick={() => {
                this.props.handleUpdateTask(this.state.taskName);
              }}
              className="ms-2"
            >
              <i className="fa fa-upload"></i>Upload task
            </Button>
          ) : (
            <Button
              onClick={() => {
                this.props.handleUpdateTask(this.state.taskName);
              }}
              className="ms-2"
            >
              <i className="fa fa-upload"></i>Upload task
            </Button>
          )}
          <Heading3>Task to do</Heading3>
          <Table>
            <Thead>{this.renderTaskToDo()}</Thead>
          </Table>
          <Heading3>Task complete</Heading3>
          <Table>
            <Thead>{this.renderTaskComplete()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    themeColor: state.ToDoListReducer.themeColor,
    taskList: state.ToDoListReducer.taskList,
    taskEdit: state.ToDoListReducer.taskEdit,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleAddTask(newTask) {
      dispatch(themTask(newTask));
    },
    handleChangeTheme(themeId) {
      dispatch(doiTheme(themeId));
    },
    handleDeleteTask(taskId) {
      dispatch(xoaTask(taskId));
    },
    handleDoneTask(taskId) {
      dispatch(xongTask(taskId));
    },
    handleRedoTask(taskId) {
      dispatch(hoanTask(taskId));
    },
    handleEditTask(task) {
      dispatch(suaTask(task));
    },
    handleUpdateTask(taskName) {
      dispatch(capNhatTask(taskName));
    },
  };
};
// đây là một lifecycle trả về props cũ và state cũ của component trước khi render nhưng chạy sau render

export default connect(mapStateToProps, mapDispatchToProps)(ToDoList);
