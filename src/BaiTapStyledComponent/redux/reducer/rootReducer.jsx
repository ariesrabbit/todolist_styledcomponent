import { combineReducers } from "redux";
import { ToDoListReducer } from "./ToDoListReducer";

export const rootReducer_ToDoListReducer = combineReducers({
  ToDoListReducer,
});
