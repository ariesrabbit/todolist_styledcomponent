import {
  ADD_TASK,
  CHANGE_THEME,
  DETELE_TASK,
  DONE_TASK,
  EDIT_TASK,
  REDO_TASK,
  UPDATE_TASK,
} from "./../constant/toDoListConstant";
import { arrTheme } from "../../Themes/ThemeManager";
import { ToDoListPrimaryTheme } from "./../../Themes/ToDoListPrimaryTheme";

const initialState = {
  themeColor: ToDoListPrimaryTheme,
  taskList: [
    { id: 1, taskName: "task 1", done: true },
    { id: 2, taskName: "task 2", done: false },
    { id: 3, taskName: "task 3", done: true },
    { id: 4, taskName: "task 4", done: false },
  ],
  taskEdit: { id: "task-0", taskName: "task 0", done: false },
};

export const ToDoListReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TASK: {
      if (payload.taskName.trim() === "") {
        alert("Task name do is require !");
        return { ...state };
      }
      let cloneTaskList = [...state.taskList];
      let index = cloneTaskList.findIndex(
        (task) => task.taskName == payload.taskName
      );
      if (index !== -1) {
        alert("Task name already exists !");
        return { ...state };
      }
      cloneTaskList.push(payload);
      state.taskList = cloneTaskList;
      return { ...state };
    }
    case CHANGE_THEME: {
      let theme = arrTheme.find((theme) => theme.id == payload);
      if (theme) {
        state.themeColor = { ...theme.theme };
      }
      return { ...state };
    }
    case DETELE_TASK: {
      let cloneTaskList = [...state.taskList];
      let index = cloneTaskList.findIndex((task) => task.id === payload);
      if (index !== -1) {
        cloneTaskList.splice(index, 1);
      }
      state.taskList = cloneTaskList;
      return { ...state };
    }
    case DONE_TASK: {
      let cloneTaskList = [...state.taskList];
      let index = cloneTaskList.findIndex((task) => task.id === payload);
      console.log("index: ", index);
      if (index !== -1) {
        cloneTaskList[index].done = true;
      }
      state.taskList = cloneTaskList;
      return { ...state };
    }
    case REDO_TASK: {
      let cloneTaskList = [...state.taskList];
      let index = cloneTaskList.findIndex((task) => task.id === payload);
      if (index !== -1) {
        cloneTaskList[index].done = false;
      }
      state.taskList = cloneTaskList;
      return { ...state };
    }
    case EDIT_TASK: {
      let cloneTaskEdit = { ...state.taskEdit };
      cloneTaskEdit = payload;
      console.log("payload: ", payload);
      state.taskEdit = cloneTaskEdit;
      return { ...state };
    }
    case UPDATE_TASK: {
      console.log(payload);
      state.taskEdit = { ...state.taskEdit, taskName: payload };
      let cloneTaskList = [...state.taskList];
      let index = cloneTaskList.findIndex(
        (task) => task.id == state.taskEdit.id
      );
      console.log(index);
      if (index !== -1) {
        cloneTaskList[index] = state.taskEdit;
        console.log(cloneTaskList);
      }
      state.taskList = cloneTaskList;
      return { ...state };
    }
    default:
      return { ...state };
  }
};
