import {
  ADD_TASK,
  CHANGE_THEME,
  DETELE_TASK,
  DONE_TASK,
  REDO_TASK,
  EDIT_TASK,
  UPDATE_TASK,
} from "./../constant/toDoListConstant";

export const themTask = (newTask) => {
  return {
    type: ADD_TASK,
    payload: newTask,
  };
};

export const doiTheme = (themeId) => {
  return { type: CHANGE_THEME, payload: themeId };
};

export const xongTask = (taskId) => {
  return {
    type: DONE_TASK,
    payload: taskId,
  };
};

export const xoaTask = (taskId) => {
  return {
    type: DETELE_TASK,
    payload: taskId,
  };
};

export const hoanTask = (taskId) => {
  return {
    type: REDO_TASK,
    payload: taskId,
  };
};

export const suaTask = (task) => {
  return {
    type: EDIT_TASK,
    payload: task,
  };
};
export const capNhatTask = (taskName) => {
  return {
    type: UPDATE_TASK,
    payload: taskName,
  };
};
