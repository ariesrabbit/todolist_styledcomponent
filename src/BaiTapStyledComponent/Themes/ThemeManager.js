import { ToDoListDarkTheme } from "./ToDoListDarkTheme";
import { ToDoListLightTheme } from "./ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "./ToDoListPrimaryTheme";

export const arrTheme = [
  { id: "1", name: "Primary Theme", theme: ToDoListPrimaryTheme },
  { id: "2", name: "Light Theme", theme: ToDoListLightTheme },
  { id: "3", name: "Dark Theme", theme: ToDoListDarkTheme },
];
