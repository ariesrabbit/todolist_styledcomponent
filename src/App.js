import "./App.css";
import ToDoList from "./BaiTapStyledComponent/TodoList/ToDoList";

function App() {
  return (
    <div>
      <ToDoList />
    </div>
  );
}

export default App;
